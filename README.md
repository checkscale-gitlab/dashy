# Dashy - Dashboard

<p align="left">
  <i>Dashy helps you organize your self-hosted services by making them accessible from a single place</i>
  <br/>
  <img width="200" src="https://i.ibb.co/yhbt6CY/dashy.png" />
</p>


## Environments

```bash
#!/usr/bin/env
TZ=Europe/Zurich

DASHY_HOSTNAME=dashy.example.com

# setup ip restriction or authentification
TRAEFIK_MIDDLEWARES=""
```
